package toolgo

import (
	"github.com/vdobler/chart"
	"github.com/vdobler/chart/imgg"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"os"
)

// Dumper helps saving plots of size WxH in a NxM grid layout
// in several formats
type Dumper struct {
	N, M, W, H, Cnt int
	I               *image.RGBA
	imgFile         *os.File
	fileName        string
}

func NewDumper(name string, n, m, w, h int) *Dumper {
	path := GetCurrentPath()
	var err error
	dumper := Dumper{N: n, M: m, W: w, H: h}
	dumper.fileName = path + name + ".png"
	dumper.imgFile, err = os.Create(dumper.fileName)
	if err != nil {
		panic(err)
	}
	dumper.I = image.NewRGBA(image.Rect(0, 0, n*w, m*h))
	bg := image.NewUniform(color.RGBA{0xff, 0x00, 0x00, 0xff})
	draw.Draw(dumper.I, dumper.I.Bounds(), bg, image.ZP, draw.Src)
	return &dumper
}

func (d *Dumper) Close() {
	png.Encode(d.imgFile, d.I)
	d.imgFile.Close()
}

func (d *Dumper) Plot(c chart.Chart) {
	row, col := d.Cnt/d.N, d.Cnt%d.N
	//背景色
	igr := imgg.AddTo(d.I, col*d.W, row*d.H, d.W, d.H, color.RGBA{0xff, 0xff, 0xff, 0xff}, nil, nil)
	c.Plot(igr)
}

func ScatterChart(x []float64, y []float64, title string) string {
	dumper := NewDumper("fps", 1, 1, 1068, 455)
	defer dumper.Close()

	pl := chart.ScatterChart{Title: ""}
	pl.XRange.Label, pl.YRange.Label = "", ""
	pl.AddDataPair("fps:"+title, x, y, chart.PlotStyleLines,
		chart.Style{Symbol: '#', SymbolColor: color.NRGBA{0x00, 0xff, 0x00, 0xff}, SymbolSize: 0.5, LineStyle: chart.SolidLine})

	pl.XRange.ShowZero = false
	pl.XRange.Time = true
	//pl.XRange.TMin = time.Unix(1623308649, 0)
	//pl.XRange.TMax = time.Unix(1623395049, 0)
	//pl.XRange.TicSetting.TFormat(time.Time{time.Now().,0,time.Now().Location()},nil)
	pl.XRange.TicSetting.Mirror = 0
	pl.YRange.TicSetting.Mirror = 0
	pl.XRange.TicSetting.Grid = 0
	pl.XRange.Label = ""
	pl.YRange.Label = ""
	//pl.Key.Cols = 10
	pl.Key.Border = -1
	pl.Key.Pos = "itr"

	dumper.Plot(&pl)

	return dumper.fileName
}
