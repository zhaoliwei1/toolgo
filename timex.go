package toolgo

import (
	"fmt"
	"sync"
	"time"
)

func SleepMs(ms time.Duration) {
	time.Sleep(ms * 1000000)
}

func Time2Unix(tm string) int64 {
	timeLayout := "2006-1-2 15:04:05"                       //转化所需模板
	loc, _ := time.LoadLocation("Local")                    //重要：获取时区
	theTime, _ := time.ParseInLocation(timeLayout, tm, loc) //使用模板在对应时区转化为time.time类型
	return theTime.Unix()                                   //转化为时间戳 类型是int64
}

func Recent7Days() (string, string, string, string, string, string, string) {
	return Day(0, 1), Day(1, 1), Day(2, 1), Day(3, 1), Day(4, 1), Day(5, 1), Day(6, 1)
}

func Day(difDay int, format int) string {
	dayUnixTime := time.Now().Unix() - (int64(difDay) * 86400)

	var day string
	if format == 1 {
		day = fmt.Sprintf("%d-%.2d-%.2d", time.Unix(dayUnixTime, 0).Year(), int(time.Unix(dayUnixTime, 0).Month()), time.Unix(dayUnixTime, 0).Day())
	} else {
		day = fmt.Sprintf("%d-%d-%d", time.Unix(dayUnixTime, 0).Year(), int(time.Unix(dayUnixTime, 0).Month()), time.Unix(dayUnixTime, 0).Day())
	}

	return day
}

func OnlyDay(difDay int, format int) string {
	dayUnixTime := time.Now().Unix() - (int64(difDay) * 86400)

	var day string
	if format == 1 {
		day = fmt.Sprintf("%.2d-%.2d", int(time.Unix(dayUnixTime, 0).Month()), time.Unix(dayUnixTime, 0).Day())
	} else {
		day = fmt.Sprintf("%d-%d", int(time.Unix(dayUnixTime, 0).Month()), time.Unix(dayUnixTime, 0).Day())
	}

	return day
}

func TodayTimeRange() (int64, int64) {
	cur := time.Now()
	y, m, d := cur.Date()

	todayBegin := fmt.Sprintf("%d-%.2d-%.2d 00:00:00", y, m, d)
	todayEnd := fmt.Sprintf("%d-%.2d-%.2d 23:59:59", y, m, d)

	return Time2Unix(todayBegin), Time2Unix(todayEnd)
}

func UnixTime2HM(ms int64) string {
	ms = ms / 1000
	t := fmt.Sprintf("%.2d:%.2d", time.Unix(ms, 0).Hour(), time.Unix(ms, 0).Minute())
	return t
}

func UnixTime2YMD(ms int64) string {
	ms = ms / 1000
	t := fmt.Sprintf("%.2d-%.2d-%.2d", time.Unix(ms, 0).Year(), time.Unix(ms, 0).Month(), time.Unix(ms, 0).Day())
	return t
}

func UnixTime2(ms int64) string {
	return time.Unix(ms/1000, 0).Format("2006-01-02 15:04:05") //转化为时间戳 类型是int64
}

func CurTimeUnixSec() int64 {
	return time.Now().Unix()
}

func Today(format int) string {
	cur := time.Now()
	y, m, d := cur.Date()

	var today string
	if format == 1 {
		today = fmt.Sprintf("%d-%.2d-%.2d", y, m, d)
	} else {
		today = fmt.Sprintf("%d-%d-%d", y, m, d)
	}

	return today
}

func CurTime(format int) string {
	cur := time.Now()
	y, m, d := cur.Date()

	var today string
	if format == 1 {
		today = fmt.Sprintf("%d-%.2d-%.2d %.2d:%.2d:%.2d", y, m, d, cur.Hour(), cur.Minute(), cur.Second())
	} else {
		today = fmt.Sprintf("%d-%d-%d %d:%d:%d", y, m, d, cur.Hour(), cur.Minute(), cur.Second())
	}

	return today
}

func Time2Week(year, month, day uint16) int {
	var y, m, c uint16
	if month >= 3 {
		m = month
		y = year % 100
		c = year / 100
	} else {
		m = month + 12
		y = (year - 1) % 100
		c = (year - 1) / 100
	}
	week := y + (y / 4) + (c / 4) - 2*c + ((26 * (m + 1)) / 10) + day - 1
	if week < 0 {
		week = 7 - (-week)%7
	} else {
		week = week % 7
	}

	return int(week)
}

/*func Time2HourMinute(ms int64) float64 {
	ms = ms / 1000
	t := float64(time.Unix(ms, 0).Hour() * 100)
	t += float64(time.Unix(ms, 0).Minute())
	return t / 100.0
}*/

func Time2HourMinute(ms int64) float64 {
	ms = ms / 1000
	t := float64(time.Unix(ms, 0).Hour() * 60)
	t += float64(time.Unix(ms, 0).Minute())
	return t
}

func CurTimeIndex(periodSec int) int64 {
	return time.Now().Unix() / int64(periodSec)
}

func TimeIndex(tm string, periodSec int) int64 {
	return Time2Unix(tm) / int64(periodSec)
}

//时间周期判定使用
var once sync.Once

var lastTimePeriod [101]int64
var GCurTimeSec int64

func refreshTime() {
	for i, _ := range lastTimePeriod {
		lastTimePeriod[i] = CurTimeUnixSec()
	}

	GCurTimeSec = CurTimeUnixSec()
	go func() {
		for {
			GCurTimeSec = CurTimeUnixSec()
			time.Sleep(200000000)
		}
	}()
}

// id 0~100
func PeriodTime(id int, periodSec int64) bool {
	once.Do(func() {
		refreshTime()
	})

	if GCurTimeSec >= lastTimePeriod[id]+periodSec {
		lastTimePeriod[id] = GCurTimeSec
		return true
	}

	return false
}
