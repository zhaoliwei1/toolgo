package toolgo

import (
	"errors"
	"reflect"
	"unsafe"
)

type hchan struct {
	qcount   uint           // total data in the queue
	dataqsiz uint           // size of the circular queue
	buf      unsafe.Pointer // points to an array of dataqsiz elements
	elemsize uint16
	closed   uint32
}

type ChanInfo struct {
	Closed bool // 是否关闭
	Len    uint // channel内数据量
	Cap    uint // channel容量
	Block  bool // 是否已经阻塞
}

func ChanStatus(c interface{}) (*ChanInfo, error) {
	v := reflect.ValueOf(c)
	if v.Type().Kind() != reflect.Chan {
		return nil, errors.New("type must be channel")
	}
	i := (*[2]uintptr)(unsafe.Pointer(&c))
	h := (*hchan)(unsafe.Pointer(i[1]))
	return &ChanInfo{
		Cap:    h.dataqsiz,
		Len:    h.qcount,
		Closed: h.closed == 1,
		Block:  h.qcount >= h.dataqsiz,
	}, nil
}
