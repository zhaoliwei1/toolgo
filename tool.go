package toolgo

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"runtime"
	"strings"
)

//获取可执行文件路径
func GetCurrentPath() string {
	file, err := exec.LookPath(os.Args[0])
	if err != nil {
		return ""
	}

	path, err := filepath.Abs(file)
	if err != nil {
		return ""
	}

	i := strings.LastIndex(path, "/")
	if i < 0 {
		i = strings.LastIndex(path, "\\")
	}

	if i < 0 {
		return ""
	}
	return path[0 : i+1]
}

//进程阻塞函数
func Block() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)

	s := <-c
	II("Got signal: %s", s)

	//defer func() { <-make(chan bool) }()
}

func SetMaxProcs() {
	cpuNum := runtime.NumCPU()
	II("tool.SetMaxProcs : %d", cpuNum)
	runtime.GOMAXPROCS(cpuNum)
}

func ReadFileX(filePath string, relative bool) string {
	if relative {
		exePath := GetCurrentPath()
		if exePath == "" {
			fmt.Println("ERROR : ReadFileX() GetCurrentPath() failed")
			return ""
		}

		filePath = exePath + filePath
	}

	f, err := os.Open(filePath)
	if err != nil {
		return ""
	}

	bys, err1 := ioutil.ReadAll(f)
	if err1 != nil {
		return ""
	}

	var str string = string(bys[:])

	return str
}

func GetFileMd5(filename string) string {
	path := fmt.Sprintf("%s", filename)
	pFile, err := os.Open(path)
	if err != nil {
		fmt.Errorf("打开文件失败，filename=%v, err=%v", filename, err)
		return ""
	}
	defer pFile.Close()
	md5h := md5.New()
	io.Copy(md5h, pFile)

	return hex.EncodeToString(md5h.Sum(nil))
}
